FROM golang:alpine AS builder
WORKDIR /app
COPY . ./
RUN go build -o main

FROM alpine
ENV HTTP_HOST=:8080
EXPOSE 8080
COPY --from=builder /app/main .
RUN apk add ca-certificates
CMD ["./main"]
