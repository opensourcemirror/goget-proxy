# goget-proxy

Vanity imports for golang. Ensures only serves existing repos with Gitea API.

## Configuration

* `GITEA_HOST` - Path to Gitea instance, used for API and generating URIs
* `GITEA_API_TOKEN` - Gitea API token to ensure repo exists
* `SHORT_HOST` - Vanity hostname
* `HTTP_HOST` - Where to listen for incoming requests
